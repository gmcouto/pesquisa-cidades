package pesquisacidade.model

import javax.persistence.CascadeType;
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.NamedAttributeNode
import javax.persistence.NamedEntityGraph
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.UniqueConstraint

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction

@Entity
@Table(
	name = "cidade",
	indexes=@Index(
		name="idx_cidade_nome",
		columnList="nome")
	)
@NamedEntityGraph(name = "Cidade.respostas", attributeNodes = @NamedAttributeNode("respostas"))
@UniqueConstraint(columnNames=["nome"])
class Cidade {
	@Id
	@GeneratedValue
	@Column(name = "cidade_id")
	private Long id

	@Column(name="nome", unique = true, nullable=false)
	private String nome
	
	@OneToMany(fetch=FetchType.LAZY, targetEntity=Resposta.class, mappedBy = "cidade")
	private List<Resposta> respostas
	private Cidade() {
	}

	public Cidade(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public List<Resposta> getRespostas() {
		return respostas;
	}
}
