package pesquisacidade.model

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

@Entity
@Table(name = "resposta", uniqueConstraints=@UniqueConstraint(name="UNIQUE_RESPOSTA_ALTERNATIVA_DE_CIDADE", columnNames=["cidade_id","alternativa_id"]))
class Resposta {
	@Id
	@GeneratedValue
	@Column(name = "resposta_id")
	private Long id

	@ManyToOne(targetEntity = Cidade.class)
	@JoinColumn(name = "cidade_id", referencedColumnName = "cidade_id", nullable=false)
	private Cidade cidade

	@ManyToOne(targetEntity = Alternativa.class)
	@JoinColumn(name = "alternativa_id", referencedColumnName = "alternativa_id", nullable=false)
	private Alternativa alternativa
	
	private Resposta() {
		
	}

	public Resposta(Cidade cidade, Alternativa alternativa) {
		super();
		this.cidade = cidade;
		if(alternativa.getPergunta()) {
			this.alternativa = alternativa;
		} else {
			throw new IllegalArgumentException("A alternativa deve possuir uma pergunta")
		}
	}

	public Alternativa getAlternativa() {
		return alternativa;
	}

	public void setAlternativa(Alternativa alternativa) {
		if(alternativa.getPergunta()) {
			this.alternativa = alternativa;
		}
	}

	public Long getId() {
		return id;
	}

	public Cidade getCidade() {
		return cidade;
	}
}
