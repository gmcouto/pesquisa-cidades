package pesquisacidade.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.NamedAttributeNode
import javax.persistence.NamedEntityGraph
import javax.persistence.OneToMany
import javax.persistence.Table

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction

@Entity
@Table(name="pergunta")
class Pergunta {
	@Id
	@GeneratedValue
	@Column(name = "pergunta_id")
	private Long id
	
	@Column(name="descricao", unique=true, nullable=false, length=500)
	private String descricao

	@OneToMany(fetch=FetchType.EAGER, targetEntity=Alternativa.class, mappedBy = "pergunta")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<Alternativa> alternativas;

	private Pergunta() {
		
	}
	public String getDescricao() {
		return descricao;
	}

	public Pergunta(String descricao) {
		super();
		this.descricao = descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public List<Alternativa> getAlternativas() {
		return alternativas;
	}
	
	
}
