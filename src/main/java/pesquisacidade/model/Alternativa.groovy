package pesquisacidade.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.NamedAttributeNode
import javax.persistence.NamedEntityGraph
import javax.persistence.OneToMany
import javax.persistence.Table

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction

@Entity
@NamedEntityGraph(name = "Alternativa.respostas", attributeNodes = @NamedAttributeNode("respostas"))
@Table(name = "alternativa")
class Alternativa {
	@Id
	@GeneratedValue
	@Column(name = "alternativa_id")
	private Long id
	
	@Column(name="descricao",nullable=false, length=500)
	private String descricao
	
	@ManyToOne(targetEntity = Pergunta.class)
	@JoinColumn(nullable=false, name = "pergunta_id", referencedColumnName = "pergunta_id")
	private Pergunta pergunta
	
	@OneToMany(fetch=FetchType.LAZY, targetEntity=Resposta.class, mappedBy = "alternativa")
	private List<Resposta> respostas;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public Pergunta getPergunta() {
		return pergunta;
	}
	
	private Alternativa() {
		
	}

	public Alternativa(String descricao, Pergunta pergunta) {
		super();
		this.descricao = descricao;
		this.pergunta = pergunta;
	}
	
	
}
