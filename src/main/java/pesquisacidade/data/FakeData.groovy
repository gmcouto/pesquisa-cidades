package pesquisacidade.data

import javax.annotation.PostConstruct

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

import pesquisacidade.model.Alternativa
import pesquisacidade.model.Cidade
import pesquisacidade.model.Pergunta
import pesquisacidade.model.Resposta
import pesquisacidade.repository.AlternativaRepository
import pesquisacidade.repository.CidadeRepository
import pesquisacidade.repository.PerguntaRepository
import pesquisacidade.repository.RespostaRepository

@Profile("fake-data")
@Service
class FakeData {
	private AlternativaRepository alternativaRepository
	private CidadeRepository cidadeRepository
	private PerguntaRepository perguntaRepository
	private RespostaRepository respostaRepository
	
	@Autowired
	public FakeData(AlternativaRepository alternativaRepository, CidadeRepository cidadeRepository,
			PerguntaRepository perguntaRepository, RespostaRepository respostaRepository) {
		super();
		this.alternativaRepository = alternativaRepository;
		this.cidadeRepository = cidadeRepository;
		this.perguntaRepository = perguntaRepository;
		this.respostaRepository = respostaRepository;
	}
			
	@PostConstruct
	void inicia() {
		dadosFalsos()
	}
	void dadosFalsos() {
		def cidade1, cidade2, cidade3, pergunta1, pergunta2, alternativa1, alternativa2, alternativa3, alternativa4, resposta1, resposta2, resposta3, resposta4
		cidade1 = cidadeRepository.save(new Cidade("Porto Alegre"))
		cidade2 = cidadeRepository.save(new Cidade("Floripa"))
		cidade3 = cidadeRepository.save(new Cidade("São Paulo"))
		pergunta1 = perguntaRepository.save(new Pergunta("Como bla bla 1?"))
		alternativa1 = alternativaRepository.save(new Alternativa("Desse jeito", pergunta1))
		alternativa2 = alternativaRepository.save(new Alternativa("Daquele jeito", pergunta1))
		pergunta2 = perguntaRepository.save(new Pergunta("O que bleh bleh 2?"))
		alternativa3 = alternativaRepository.save(new Alternativa("Desse jeito", pergunta2))
		alternativa4 = alternativaRepository.save(new Alternativa("Daquele jeito", pergunta2))
		resposta1 = respostaRepository.save(new Resposta(cidade1, alternativa1))
		resposta2 = respostaRepository.save(new Resposta(cidade2, alternativa2))
		resposta3 = respostaRepository.save(new Resposta(cidade3, alternativa1))
		resposta4 = respostaRepository.save(new Resposta(cidade1, alternativa3))
		def resposta5 = respostaRepository.save(new Resposta(cidade1, alternativa2))//should fail
		
		def Cidade resultado = cidadeRepository.findByNomeOrderByNomeAsc("Porto Alegre")
		
		println resultado.id+" "+resultado.nome
		resultado.respostas?.each { resposta ->
			System.out.println resposta.alternativa.pergunta.descricao + " -> " + resposta.alternativa.descricao
		}
		
		def List<Alternativa> alternativas = alternativaRepository.respondidasPorCidade(resultado)
		alternativas?.each { alternativa ->
			println alternativa.pergunta.descricao + " -> " + alternativa.descricao
		}
	}
}
