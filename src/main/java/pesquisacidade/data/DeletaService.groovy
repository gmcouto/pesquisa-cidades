package pesquisacidade.data

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import pesquisacidade.model.Alternativa
import pesquisacidade.model.Cidade
import pesquisacidade.model.Pergunta
import pesquisacidade.model.Resposta
import pesquisacidade.repository.AlternativaRepository
import pesquisacidade.repository.CidadeRepository
import pesquisacidade.repository.PerguntaRepository
import pesquisacidade.repository.RespostaRepository

@Service
class DeletaService {
	@Autowired
	CidadeRepository cidadeRepository
	@Autowired
	PerguntaRepository perguntaRepository
	@Autowired
	RespostaRepository respostaRepository
	@Autowired
	AlternativaRepository alternativaRepository
	
	public void deletaCidade(Cidade c){
		List<Resposta> respostas = respostaRepository.findByCidade(c)
		respostas?.each {respostaRepository.delete(it)}
		cidadeRepository.delete(c);
	}
	
	public void deletaAlternativa(Alternativa a){
		List<Resposta> respostas = respostaRepository.findByAlternativa(a)
		respostas?.each {respostaRepository.delete(it)}
		alternativaRepository.delete(a)
	}
	
	public void deletaPergunta(Pergunta p) {
		List<Alternativa> alternativas = alternativaRepository.findByPergunta(p)
		alternativas?.each { deletaAlternativa(it) }
		perguntaRepository.delete(p)
	}
}
