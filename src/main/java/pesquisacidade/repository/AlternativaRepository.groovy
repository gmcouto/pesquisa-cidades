package pesquisacidade.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

import pesquisacidade.model.Alternativa
import pesquisacidade.model.Cidade
import pesquisacidade.model.Pergunta

@Repository
interface AlternativaRepository extends CrudRepository<Alternativa, Long> {
	List<Alternativa> findByPergunta(Pergunta pergunta);
	
	@Query("select r.alternativa from Resposta r where r.cidade = ?1")
	List<Alternativa> respondidasPorCidade(Cidade cidade)
}
