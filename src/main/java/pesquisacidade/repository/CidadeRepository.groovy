package pesquisacidade.repository


import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

import pesquisacidade.model.Cidade

@Repository
interface CidadeRepository extends CrudRepository<Cidade, Long> {
	@EntityGraph(value = "Cidade.respostas", type = EntityGraphType.LOAD)
	Cidade findByNomeOrderByNomeAsc(String nome)
	
	List<Cidade> findByOrderByNomeAsc()
}
