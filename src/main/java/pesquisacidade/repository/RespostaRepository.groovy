package pesquisacidade.repository

import javax.transaction.Transactional

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

import pesquisacidade.model.Alternativa
import pesquisacidade.model.Cidade
import pesquisacidade.model.Resposta

@Repository
interface RespostaRepository extends CrudRepository<Resposta, Long> {
	@Modifying
	@Transactional
	@Query("delete from Resposta r where r.cidade = :cidade AND r.alternativa in :alternativas")
	void deletarTodasRespostasDeUmaCidadePorAlternativas(@Param("cidade") Cidade cidade,@Param("alternativas") List<Alternativa> alternativas);
	
	List<Resposta> findByCidade(Cidade c);
	List<Resposta> findByAlternativa(Alternativa c);
}
