package pesquisacidade.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository;

import pesquisacidade.model.Pergunta

@Repository
interface PerguntaRepository extends CrudRepository<Pergunta, Long> {

}
