package pesquisacidade.web.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

import groovy.transform.AutoClone
import pesquisacidade.data.DeletaService;
import pesquisacidade.model.Alternativa
import pesquisacidade.model.Pergunta
import pesquisacidade.repository.AlternativaRepository
import pesquisacidade.repository.PerguntaRepository

@Controller
class AlternativaController {
	@Autowired
	PerguntaRepository perguntaRepository

	@Autowired
	AlternativaRepository alternativaRepository
	
	@Autowired
	DeletaService deletaService

	@RequestMapping(value="/perguntas/{perguntaId}",method=RequestMethod.GET)
	String lista(@PathVariable(value="perguntaId") Long perguntaId, Model model) {
		Pergunta pergunta = perguntaRepository.findOne(perguntaId)
		if (!pergunta)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		model.addAttribute("pergunta",pergunta)
		model.addAttribute("alternativas",alternativaRepository.findByPergunta(pergunta))
		return "alternativa_lista";
	}

	@RequestMapping(value = "/perguntas/{perguntaId}",method=RequestMethod.POST)
	@ResponseBody
	String insere(@PathVariable(value="perguntaId") Long perguntaId, @RequestParam("alternativa") String alternativa) {
		Pergunta pergunta = perguntaRepository.findOne(perguntaId)
		if (!pergunta)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			
		if (!alternativa || alternativa.empty)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		
		println "criando alternativa '$alternativa'"
		Alternativa criada = alternativaRepository.save(new Alternativa(alternativa, pergunta))
		return criada.getId()
	}

	@RequestMapping(value = "/perguntas/{perguntaId}",method=RequestMethod.PUT)
	@ResponseBody
	String edita(@RequestBody MultiValueMap<String,String> body) {
		Long alternativaId = body.getFirst("pk") as Long
		String alternativa = body.getFirst "value"
		
		Alternativa encontrada = alternativaRepository.findOne(alternativaId)
		if (!encontrada || !alternativa || alternativa.empty)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			
		println "alterando a pergunta '$alternativaId' para '$alternativa'"
		encontrada.setDescricao(alternativa)
		encontrada = perguntaRepository.save(encontrada)
		
		return encontrada.getDescricao()
	}

	@RequestMapping(value = "/perguntas/{perguntaId}",method=RequestMethod.DELETE)
	@ResponseBody
	String deleta(@RequestBody MultiValueMap<String,String> body) {
			
		Long alternativaId = body.getFirst("id") as Long
		Alternativa encontrada = alternativaRepository.findOne(alternativaId)
		if (!encontrada)
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		
		println "deletando a pergunta '$alternativaId'"
		deletaService.deletaAlternativa(encontrada)
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}
}
