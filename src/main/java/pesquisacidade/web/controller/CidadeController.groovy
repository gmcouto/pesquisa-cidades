package pesquisacidade.web.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

import pesquisacidade.data.DeletaService;
import pesquisacidade.model.Alternativa
import pesquisacidade.model.Cidade
import pesquisacidade.model.Pergunta
import pesquisacidade.model.Resposta
import pesquisacidade.repository.AlternativaRepository
import pesquisacidade.repository.CidadeRepository
import pesquisacidade.repository.PerguntaRepository
import pesquisacidade.repository.RespostaRepository

@Controller
class CidadeController {
	@Autowired
	CidadeRepository cidadeRepository
	@Autowired
	PerguntaRepository perguntaRepository
	@Autowired
	RespostaRepository respostaRepository
	@Autowired
	AlternativaRepository alternativaRepository
	@Autowired
	DeletaService deletaService
	@RequestMapping(value="/cidades",method=RequestMethod.GET)
	String lista(Model model) {
		model.addAttribute("cidades",cidadeRepository.findByOrderByNomeAsc())
		return "cidade_lista";
	}

	@RequestMapping(value="/cidades/{cidadeId}",method=RequestMethod.GET)
	String responder(@PathVariable(value="cidadeId") Long cidadeId, Model model) {
		println "procurando a cidade "+cidadeId
		Cidade cidade = cidadeRepository.findOne(cidadeId);
		if(cidade){
			println "encontrado a cidade "+cidade.nome
			model.addAttribute("cidade",cidade)
			model.addAttribute("alternativas",alternativaRepository.respondidasPorCidade(cidade))
			model.addAttribute("perguntas",perguntaRepository.findAll())
			return "responder";
		} else {
		 println "não encontrado"
		 return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value="/cidades/{cidadeId}",method=RequestMethod.POST)
	@ResponseBody
	String insereResposta(@PathVariable(value="cidadeId") Long cidadeId,
			@RequestParam(value="perguntas[]", required=false) List<Long> perguntasIds,
			@RequestParam(value="alternativas[]",required=false) List<Long> alternativasIds,
			Model model) {
		Cidade cidade = cidadeRepository.findOne cidadeId
		if (!cidade)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);

		if (perguntasIds) {
			List<Pergunta> perguntas = perguntaRepository.findAll perguntasIds
			perguntas.each { pergunta ->
				respostaRepository.deletarTodasRespostasDeUmaCidadePorAlternativas(cidade,pergunta.alternativas)
			}
		}
		List<Alternativa> alternativas = alternativaRepository.findAll(alternativasIds)
		alternativas.each { alternativa ->
			respostaRepository.save(new Resposta(cidade,alternativa));
		}
		return "responder";
	}


	@RequestMapping(value = "/cidades",method=RequestMethod.POST)
	@ResponseBody
	String insere(@RequestParam("cidade") String nome) {
		System.out.println "criando cidade com nome '$nome'"
		if (!nome || nome.empty)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		Cidade encontrada = cidadeRepository.save(new Cidade(nome))
		return encontrada.getId()
	}

	@RequestMapping(value = "/cidades",method=RequestMethod.PUT)
	@ResponseBody
	String edita(@RequestBody MultiValueMap<String,String> body) {
		Long cidadeId = body.getFirst("pk") as Long
		String cidade = body.getFirst "value"
		System.out.println "alterando a cidade '$cidadeId' para '$cidade'"
		Cidade encontrada = cidadeRepository.findOne(cidadeId)
		if (!encontrada || !cidade || cidade.empty)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		encontrada.nome = cidade
		encontrada = cidadeRepository.save(encontrada)
		return encontrada.nome
	}

	@RequestMapping(value = "/cidades",method=RequestMethod.DELETE)
	@ResponseBody
	String deleta(@RequestBody MultiValueMap<String,String> body) {
		Long cidadeId = body.getFirst("id") as Long
		System.out.println "deletando a cidade '$cidadeId'"
		Cidade encontrada = cidadeRepository.findOne(cidadeId)
		if (!encontrada)
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		deletaService.deletaCidade(encontrada)
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}
}
