package pesquisacidade.web.controller

import javax.servlet.http.HttpServletRequest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

import pesquisacidade.data.DeletaService;
import pesquisacidade.model.Pergunta
import pesquisacidade.repository.PerguntaRepository

@Controller
class PerguntaController {
	@Autowired
	PerguntaRepository perguntaRepository
	@Autowired
	DeletaService deletaService
	
	@RequestMapping(value="/perguntas",method=RequestMethod.GET)
	String lista(Model model) {
		model.addAttribute("perguntas",perguntaRepository.findAll())
	  return "pergunta_lista";
	}
	
	@RequestMapping(value = "/perguntas",method=RequestMethod.POST)
	@ResponseBody
	String insere(@RequestParam("pergunta") String pergunta) {
		System.out.println "criando pergunta '$pergunta'"
		if (!pergunta || pergunta.empty)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		Pergunta encontrada = perguntaRepository.save(new Pergunta(pergunta))
		return encontrada.getId()
	}
	
	@RequestMapping(value = "/perguntas",method=RequestMethod.PUT)
	@ResponseBody
	String edita(@RequestBody MultiValueMap<String,String> body) {
		Long perguntaId = body.getFirst("pk") as Long
		String pergunta = body.getFirst "value"
		System.out.println "alterando a pergunta '$perguntaId' para '$pergunta'"
		Pergunta encontrada = perguntaRepository.findOne(perguntaId)
		if (!encontrada || !pergunta || pergunta.empty)
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		encontrada.setDescricao(pergunta)
		encontrada = perguntaRepository.save(encontrada)
		return encontrada.getDescricao()
	}
	
	@RequestMapping(value = "/perguntas",method=RequestMethod.DELETE)
	@ResponseBody
	String deleta(@RequestBody MultiValueMap<String,String> body) {
		Long perguntaId = body.getFirst("id") as Long
		System.out.println "deletando a pergunta '$perguntaId'"
		Pergunta encontrada = perguntaRepository.findOne(perguntaId)
		if (!encontrada)
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		deletaService.deletaPergunta(encontrada)
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}
}
