package pesquisacidade

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@SpringBootApplication
@ComponentScan
@Controller
class DemoApplication {
	@RequestMapping("/")
	String home() {
	  return "index";
	}
	@RequestMapping("/login")
	String login() {
	  return "login";
	}
	@RequestMapping("/logout")
	String logout() {
	  return "login";
	}
    static void main(String[] args) {
        SpringApplication.run DemoApplication, args
    }
}
